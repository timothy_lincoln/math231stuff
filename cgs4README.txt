cgs4.m Readme

cgs4.m 
Uses Classical Gram Schmidt Process to create an orthogonal matrix from a matrix. This program also computes Q'Q = I to test for matrix orthogonality. This script also uses matlabs QR composition function to compute Q so the user can compare the two results.
  Input:
    matrix A.
  Output 
    orthogonal matrix Q
 By Timothy Lincoln
 Last Updated 9/22/2015 10:42pm


If you do not need to run the matrix with the machine epsilon in it then comment out line 18 make sure line 19 is uncommented. If you want the opposite then comment out line 19 and uncomment line 18.

