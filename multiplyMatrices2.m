%% function multmatrix = multiplyMatrices2(A, B)
% multiplies two compatible matrices and returns their product using a a function that multiplies a matrix with a vector
% Inputs 
% matrix A
% matrix B
% Outputs
% matrix AB or BA depending on the order
% Tim Lincoln Last Updated 9/7/2015

function multMatrix2=multiplyMatrices2(A, B)
%% function multmatrix2 = multiplyMatrices2(A, B)
% multiplies two compatible matrices and returns their product using a a function that multiplies a matrix with a vector
% Inputs 
% matrix A
% matrix B
% Outputs
% matrix AB or BA depending on the order
% Tim Lincoln Last Updated 9/7/2015
    
    
sizeofA = size(A);
sizeofB = size(B);
  
for( j = 1: sizeofB(1,2))
    z(:, j)= multiplyMatrixVector2(A, B, j);
  
     end
    multMatrix2 = z;
    
end