%% function multMatrixVector = multiplyMatrixVector2(A, B,j)
% multiplies a matrix with a column vector 
% Inputs: 
% matrix A
% column vector B
% index j = 1 when using this function alone always use j =1
% Outputs :
% matrix AB 
% Tim Lincoln Last Updated 9/7/2015
function multMatrixVector=multiplyMatrixVector2(A, B, j)
%% function multMatrixVector = multiplyMatrixVector2(A, B,j)
% multiplies a matrix with a column vector 
% Inputs: 
% matrix A
% column vector B
% index j = 1 when using this function alone always use j =1
% Outputs :
% matrix AB 
% Tim Lincoln Last Updated 9/7/2015
    

sizeofA = size(A);
sizeofB = size(B);

H = transpose(B);
 for(i= 1:sizeofA(1,1))
         C= A(i, :) .* H(j,:);
         G(i,:) = sum(C)  ;
      end 
    multMatrixVector = G;
end