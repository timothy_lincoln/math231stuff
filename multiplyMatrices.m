%% function multmatrix = multiplyMatrices(A, B)
% multiplies two compatible matrices and returns their product.
% Inputs 
% matrix A
% matrix C
% Outputs
% matrix AB or BA depending on the order
% Tim Lincoln Last Updated 9/7/2015
function multmatrix = multiplyMatrices(A, B)
%% function multmatrix = multiplyMatrices(A, B)
% multiplies two compatible matrices and returns their product.
% Inputs 
% matrix A
% matrix C
% Outputs
% matrix AB or BA depending on the order
% Tim Lincoln Last Updated 9/7/2015
    
i=0;
j=0;
sizeofA = size(A); %size of the matrix returns [rows, columns]
sizeofB = size(B); 


if(sizeofA(1,2) ~= sizeofB(1,1))
   disp('incompatible size of matrices')
else
    disp('compatible sizes')
    C = zeros(sizeofA(1,1) , sizeofB(1,2));
end

C= transpose(B);
for (j = 1: sizeofA(1,1))

    % disp(j)
      for( i = 1: sizeofB(1,2))
          H = A(j,:).*C(i, :);
          entry5= sum(H);    
          D(j,i) = entry5;      % sums in the corresponding entries
          
      end 
     
end
multmatrix= D;
end

