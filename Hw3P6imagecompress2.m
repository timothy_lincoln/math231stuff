% ImageCompress2
% Timothy Lincoln
% Created 9-14-2015
% Computes the singularvalue decomposition of a two greyscale images and 
% shows the rank 1 5 and 10 approximations for each image along with the 
% original image for comparison. 

%Image1: mantis.jpg     detailed image of a preying mantis
%Image2: downtown.jpg   simple image of downtown Merced Ca

% To view the Praying mantis and see related figures comment out lines.
% 17, 56 and 62
% To view downtown Merced and related figures comment out lines.
% 16, 55 and 61

RGB = imread('mantis.jpg');
%RGB = imread('downtown.jpg');

% Convert the image to Grayscale
I = rgb2gray(RGB);
% store Grayscale image in variable Gray
Grey = I;
% Converts to a number between 0 and 1
I = im2double(I);

% returns the singular values of Matrix I
[ U, S,V] = svd(I);

 % U is an orthogonal matrix
 % V is an orthogonal matrix that are the eigenvalues of I'I where  I' is
 % the transpose. 
 
% store diagonal entries of S
singularval = diag(S);

rank = [1 5 10]
% cycle through the different rank approximations.
for i = 1:length(rank)
    
% Keep largest singular valuee.
approx_singval = singularval; 
approx_singval(rank(i):end) = 0;

ns = length(singularval);
% Create the singular value matrix
approx_S = S; 
approx_S(1:ns, 1:ns) = diag(approx_singval);


% Compute low-rank approximation by multiplying out component matrices.
approx_picture = U * approx_S * V';

%display figure for rank i plot.
figure;
imshow(approx_picture), title(sprintf('Rank %d PrayingMantis', rank(i)));
%imshow(approx_picture), title(sprintf('Rank %d DowntownMerced', rank(i)));
end

%display figure of original greyscale image.
 figure;
 imshow(Grey),  title(sprintf('Original Picture PreyingMantis', rank(i)))
 %imshow(Grey),  title(sprintf('Original Picture DowntownMerced', rank(i)))
%display singular value log10 plot showing the magnitude of the singular values
 figure; plot(log10(singularval)); title('Singular Values (Log10 Scale)');