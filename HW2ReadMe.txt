ReadMe for HW 2problem 6a-c Matlab programming assignment

file multiplyMatrixVector2.m    problem 6a
file multiplymatrices.m         problem 6b
file multiplymarices2.m         problem 6c


multiplyMatrixVector2.m is a matlab function that multiplies a matrix with a column vector. 

multiplymatrices.m  is a matlab function that utilizes two nested for loops to multiply two compatible matrices.

multiplymarices2.m is a matlab function that utilzes the function  multiplyVectorMatrix.m to multiply compatible matrices column vector by column vector since the multiply matrix function multiplies matrices with column vectors. This allows one for loop to be used in this function, the other for loop is inside the function multiplyMatrixVector2.m