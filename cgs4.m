%  cgs4.m 
%  This script uses Classical Gram Schmidt Process to create an orthogonal matrix from a matrix. Computes Q'Q=I to test for orthogonality and 
%  uses MatLab QR to compute Q to allow the user to compare the Q
%  Input:
%    matrix A.
%  Output 
%    orthogonal matrix Q
%
% Last Updated 9/22/2015 10:42pm

clear
clc
%coefficient in front of machine epsilon
c = 0.05;
% machine epsilon
epsilon = 2.2204*(10^-16);
Cepsilon = c*epsilon;
% A = [1 1 , 1 ; Cepsilon 0, 0; 0  Cepsilon 0 ; 0 0 Cepsilon];
%A = rand(3,3)
A = [ 2 1 ; 3 4 ;8 9]
%size of matrix A
sizeofA= size(A);  
% intialize vector D. Vector D stores the dot product.
D= zeros(1,sizeofA(1,2));
% cycle through the vectors j
for(j = 1: sizeofA(1,2))
    % The first case where the first vector is chosen 
    switch j
        case 1
            % calculate length of the first vector
            C = (A(:,1)'*A(:,1))^0.5;
            if(C == 0)
                disp('error divide by zero')
                break
            end
            % normalize first vector thus making it orthonormal.
            Q(:,j)= A(:,j)/C;
        otherwise
                 % intialize  vector that saves the projections
                C = zeros(sizeofA(1,1),1);
                % cycle through the projections
                for(i =1:j-1)
                    % dot product Qj-1 by vector Aj (projection of Aj onto
                    % Qj-1
                    D = A(:,j)'*Q(:,i);
                    %display current projection
                    %add projection to the previously calculated
                    %projections for vector Aj
                    C(:,1) = C(:,1) + D*Q(:,i);
                end   
                      % subtract projections from original vector
                      Q(:,j)= A(:,j) - C(:,1);
                      % normalize the vector                    
                      length = (Q(:,j)'*Q(:,j))^0.5 ;
                      %check for division by zero
                      length = 0
                      if(length == 0)
                          
                        disp('error divide by zero')
                      break
                      end
                      % normalize vector to make it orthonormal
                      Q(:,j) = Q(:,j)/length;

                      
      end
end
% display Output Q obtained using Classical Gram Schmidt
disp('CGS Q')
disp(Q)

%check if first three column vectors are orthogonal
CGSorthogonality1 =Q(:,1)'*Q(:,2)
CGSorthogonality2 =Q(:,1)'*Q(:,3)
CGSorthogonality3 =Q(:,2)'*Q(:,3)

 % display Matlabs Orthogonal matrix obtained with QR decomposition
 disp('MatlabQ')
 [ U, V] = qr(A);
 disp(U)
 
 % check matlabs orthogonality from orthogonal matrix using QR
 matlabQRorthogonality1 =U(:,1)'*U(:,2)
 matlabQRorthogonality1 =U(:,2)'*U(:,3)
 
 %Test for Orthogonality  Q^TQ = I by definition for orthogonal matrices
 MatrixOrthogonality = Q'*Q
 

    